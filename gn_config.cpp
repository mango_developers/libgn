#include "resources_manager.h"

uint32_t hn_emu_create_config();
uint32_t read_num(unsigned long long *number, char *result);
uint32_t read_num(uint32_t *number, char *result);
std::string get_families ();
std::string get_models (uint32_t family, uint32_t *model_default);

using namespace rapidxml;

int main(int argc, char*argv[]) {

    hn_emu_create_config();
    return 0;
}

uint32_t hn_emu_create_config() {

    xml_document<> doc;

// xml declaration
    xml_node<> *decl = doc.allocate_node(node_declaration);
    decl->append_attribute(doc.allocate_attribute("version", "1.0"));
    decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
    doc.append_node(decl);

// root node
    std::string input;
    std::cin.get();

    uint32_t system_id = 0;
    char c_system_id[5] = "";
    std::cout << "System id [default = " << system_id << "]: ";
    read_num(&system_id, c_system_id);

    uint32_t arch_id = 0;
    char c_arch_id[3] = "";
    std::cout << "Architecture id [default = " << arch_id << "]: ";
    read_num(&arch_id, c_arch_id);


    uint32_t read_cluster_bw = BW_CL;
    char c_read_cluster_bw[12] = "";
    std::cout << "Read cluster bandwidth in MB [default = " << read_cluster_bw << "]: ";
    read_num(&read_cluster_bw, c_read_cluster_bw);

    uint32_t write_cluster_bw = BW_CL;
    char c_write_cluster_bw[12] = "";
    std::cout << "Write cluster bandwidth in MB [default = " << write_cluster_bw << "]: ";
    read_num(&write_cluster_bw, c_write_cluster_bw);

    uint32_t num_cols = 1, num_rows = 1;
    char c_num_cols[3] = "", c_num_rows[3] = "";
    std::cout << "Number elements in row [default = " << num_cols << "]: ";
    read_num(&num_cols, c_num_cols);

    std::cout << "Number rows of elements [default = " << num_rows << "]: ";
    read_num(&num_rows, c_num_rows);

    xml_node<> *root = doc.allocate_node(node_element, "system");
    root->append_attribute(doc.allocate_attribute("arch_id", c_arch_id));
    root->append_attribute(doc.allocate_attribute("read_cluster_bw", c_read_cluster_bw));
    root->append_attribute(doc.allocate_attribute("write_cluster_bw", c_write_cluster_bw));
    root->append_attribute(doc.allocate_attribute("num_rows", c_num_rows));
    root->append_attribute(doc.allocate_attribute("num_cols", c_num_cols));
    doc.append_node(root);

    auto num_tiles = num_rows * num_cols;

    uint32_t tile = 0;
    unsigned long long last_addr = 0;
    char c_tile[num_tiles][3];
    char c_family[num_tiles][3];
    char c_model[num_tiles][3];
    char c_mem_size[num_tiles][12];
    char c_read_mem_bw[num_tiles][12];
    char c_write_mem_bw[num_tiles][12];
    char c_north_port_bw[num_tiles][12];
    char c_west_port_bw[num_tiles][12];
    char c_east_port_bw[num_tiles][12];
    char c_south_port_bw[num_tiles][12];
    char c_local_port_bw[num_tiles][12];

//tiles
    std::string str_families = get_families();
    for (auto i = 0; i < num_rows; i++) {
        std::cout << std::endl << "Row   " << i + 1 << "/" << num_rows << std::endl;
        for (auto j = 0; j < num_cols; j++) {
            std::cout << std::endl << "Tile " << tile << std::endl;
            sprintf(c_tile[tile], "%d", tile);

            uint32_t family = HN_TILE_FAMILY_NONE;
            std::cout << "Tile family id  "<< str_families;
            read_num(&family, c_family[tile]);

            uint32_t model = HN_TILE_MODEL_NONE;
            std::string str_models = get_models(family, &model);
            std::cout << "Tile model id "<< str_models;
            read_num(&model, c_model[tile]);

            unsigned long long mem_size = 0;
            std::cout << "Memory size in MB [default = " << mem_size << "]: ";
            read_num(&mem_size, c_mem_size[tile]);

            uint32_t read_mem_bw = 0;
            uint32_t write_mem_bw = 0;
            if (mem_size > 0) {
                read_mem_bw = BW_MEM;
                std::cout << "Read memory bandwidth in MB [default = " << read_mem_bw << "]: ";
                read_num(&read_mem_bw, c_read_mem_bw[tile]);

                write_mem_bw = BW_MEM;
                std::cout << "Write memory bandwidth in MB [default = " << write_mem_bw << "]: ";
                read_num(&write_mem_bw, c_write_mem_bw[tile]);

            } else {
                sprintf(c_read_mem_bw[tile], "%d", 0);
                sprintf(c_write_mem_bw[tile], "%d", 0);
            }


            uint32_t north_port_bw = BW;
            std::cout << "North port bandwidth in MB [default = " << north_port_bw << "]: ";
            read_num(&north_port_bw, c_north_port_bw[tile]);

            uint32_t west_port_bw = BW;
            std::cout << "West port bandwidth in MB [default = " << west_port_bw << "]: ";
            read_num(&west_port_bw, c_west_port_bw[tile]);

            uint32_t east_port_bw = BW;
            std::cout << "East port bandwidth in MB [default = " << east_port_bw << "]: ";
            read_num(&east_port_bw, c_east_port_bw[tile]);

            uint32_t south_port_bw = BW;
            std::cout << "South port bandwidth in MB [default = " << south_port_bw << "]: ";
            read_num(&south_port_bw, c_south_port_bw[tile]);


            uint32_t local_port_bw = BW;
            std::cout << "Local port bandwidth in MB [default = " << local_port_bw << "]: ";
            read_num(&local_port_bw, c_local_port_bw[tile]);

            // child node
            xml_node<> *child = doc.allocate_node(node_element, "tile");
            child->append_attribute(doc.allocate_attribute("id", c_tile[tile]));
            child->append_attribute(doc.allocate_attribute("family", c_family[tile]));
            child->append_attribute(doc.allocate_attribute("model", c_model[tile]));
            child->append_attribute(doc.allocate_attribute("mem_size", c_mem_size[tile]));
            child->append_attribute(doc.allocate_attribute("read_mem_bw", c_read_mem_bw[tile]));
            child->append_attribute(doc.allocate_attribute("write_mem_bw", c_write_mem_bw[tile]));
            child->append_attribute(doc.allocate_attribute("north_port_bw", c_north_port_bw[tile]));
            child->append_attribute(doc.allocate_attribute("west_port_bw", c_west_port_bw[tile]));
            child->append_attribute(doc.allocate_attribute("east_port_bw", c_east_port_bw[tile]));
            child->append_attribute(doc.allocate_attribute("south_port_bw", c_south_port_bw[tile]));
            child->append_attribute(doc.allocate_attribute("local_port_bw", c_local_port_bw[tile]));
            root->append_node(child);

            tile++;
        }
    }

    std::string xml_as_string;
    print(std::back_inserter(xml_as_string), doc);

    // Save to file
    std::ofstream file(MANGO_CONFIG_XML);
    file << doc;
    file.close();
    doc.clear();
    return 0;
}


uint32_t read_num(uint32_t *number, char *result) {

    std::string input;
    std::string::size_type sz;
    std::getline(std::cin, input);
    if (!input.empty()) {
        *number = (uint32_t) std::stoul(input, &sz);
    }
    sprintf(result, "%d", *number);
    return 0;
}

uint32_t read_num(unsigned long long *number, char *result) {

    std::string input;
    std::string::size_type sz;
    std::getline(std::cin, input);
    if (!input.empty()) {
        *number = std::stoull(input, &sz);
    }
    sprintf(result, "%llu", *number);
    return 0;
}


std::string gn_to_str_unit_family() {




}

std::string get_families (){

    char result[100];

    sprintf(result, "[%d: PEAK \t %d: NUPLUS \t %d: DCT \t %d: TETRAPOD \t %d: GN EMULATION, \t DEFAULT: GN EMULATION]: ",
            HN_TILE_FAMILY_PEAK,
            HN_TILE_FAMILY_NUPLUS,
            HN_TILE_FAMILY_DCT,
            HN_TILE_FAMILY_TETRAPOD,
            HN_TILE_FAMILY_GN,
            HN_TILE_FAMILY_NONE
    );

    return result;
}


std::string gn_peak_to_str_unit_model(uint32_t *model_default) {

    *model_default = HN_PEAK_MANYCORE_0;
    char result[100];

    sprintf(result, "[%d: PMC2 \t %d: PMC4 \t %d: PMC8 \t %d: PMC8 \t %d: PMC8 \t %d: PMC8 \t %d: PMC8, \t default = PMC2]: ",
            HN_PEAK_MANYCORE_0,
            HN_PEAK_MANYCORE_1,
            HN_PEAK_MANYCORE_2,
            HN_PEAK_MANYCORE_3,
            HN_PEAK_MANYCORE_4,
            HN_PEAK_MANYCORE_5,
            HN_PEAK_MANYCORE_6
    );


    return result;
}


std::string gn_dct_to_str_unit_model(uint32_t *model_default) {

    *model_default = HN_DCT_MODEL_BASE;
    char result[100];

    sprintf(result, "[%d: DCT0, \t default = DCT0]: ",
            HN_DCT_MODEL_BASE
    );
    return result;
}

std::string gn_nuplus_to_str_unit_model(uint32_t *model_default) {

    *model_default = HN_NUPLUS_MODEL_BASE;
    char result[100];

    sprintf(result, "[%d: NU+B, \t default = NU+B]: ",
            HN_NUPLUS_MODEL_BASE
    );
    return result;
}


std::string gn_tetrapod_to_str_unit_model(uint32_t *model_default) {

    *model_default = HN_TETRAPOD_MODEL_DE4WI4;
    char result[100];

    sprintf(result, "[%d: TTDE4WI4, \t default = TTDE4WI4]: ",
            HN_TETRAPOD_MODEL_DE4WI4
    );
    return result;
}

std::string get_models (uint32_t family, uint32_t *model_default){

    switch(family) {
        case HN_TILE_FAMILY_PEAK     : return gn_peak_to_str_unit_model(model_default);
        case HN_TILE_FAMILY_NUPLUS   : return gn_nuplus_to_str_unit_model(model_default);
        case HN_TILE_FAMILY_DCT      : return gn_dct_to_str_unit_model(model_default);
        case HN_TILE_FAMILY_TETRAPOD : return gn_tetrapod_to_str_unit_model(model_default);
        default                      : return "[default = GN EMULATION]: ";
    }
}




