#include <iostream>
#include "../hn_include/hn.h"
#define VERBOSE
int main(int argc, char **argv) {


    int i;
    int entry;
    uint32_t request_type[50000];
    unsigned long long request_bw[50000];
    uint32_t request_num_tiles[50000];
    uint32_t src_tile[50000];
    uint32_t dst_tile[50000];
    uint32_t mem_tile[50000];
    uint32_t tiles_dst_ptr[50000][10];
    uint32_t types_dst_ptr[50000][10];
    unsigned long long size[50000];
    unsigned int tile_mem[50000];
    unsigned int starting_address[50000];
    unsigned int mapped[50000];
    unsigned int num_iterations;
    unsigned int num_mapped_segments;
    unsigned int successful_mappings;
    unsigned int attempted_mappings;
    unsigned int tile;
    unsigned int addr;
    unsigned long long bw;
    uint32_t types[64];

    uint32_t x;
    uint32_t y;
    uint32_t num_mappings = 0;



    hn_filter_t filter;
    if (hn_initialize(filter, 1, 1, 0, 0) != HN_SUCCEEDED) {
        printf("Error initializing the HN system\n");
        exit(1);
    };

    // Information
    printf("\n");
    printf("Collecting information:\n");
    uint32_t num_tiles;
    uint32_t num_tiles_x;
    uint32_t num_tiles_y;
    hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
    printf("  - num tiles : %d\n", num_tiles);
    printf("  - geometry  : %dx%x\n", num_tiles_x, num_tiles_y);

    printf("\n");

    // Test 1: We request a memory from each tile with memory segment of size 1MB
    printf("Test1: Requesting (finding, not assigning it) a memory multiple times, one from each tile within the HN system\n");
    printf("       The closest one must be provided with enough memory. We request a 1MB of memory each time\n");
    printf("--------------------------------------------------------------------------------------------------------------\n");
    attempted_mappings = 0;
    successful_mappings = 0;
    for (i = 0; i < num_tiles; i++) {
        unsigned long long size = 1024 * 1024;
        attempted_mappings++;
        if (hn_find_memory(i, size, &tile, &addr) == HN_SUCCEEDED) {
            successful_mappings++;
#ifdef VERBOSE
            printf("From TILE %2d: Memory at tile %2d found, with address range [%08x-%08x]\n", i, tile, addr,
                   addr + size - 1);
#endif
        } else {
#ifdef VERBOSE
            printf("From TILE %2d: Memory not found\n", i);
#endif
        }
    }
    printf("successful requests: %d, attempted requests: %d\n", successful_mappings, attempted_mappings);
    printf("done\n");

    // Test 2: We ask randomly for memory segment maps and unmaps
    num_iterations = 1000;

    printf("\n\n");
    printf("Test2: Random mapping/unmapping of random memory segments\n");
    printf("       Performing %d iterations\n", num_iterations);
    printf("--------------------------------------------------------------------------------------------------------------\n");
    for (i = 0; i < 50000; i++) mapped[i] = 0;
    num_mapped_segments = 0;
    successful_mappings = 0;
    attempted_mappings = 0;
    for (i = 0; i < num_iterations; i++) {
        entry = random() % 50000;

        if (mapped[entry]) {
            hn_release_memory(tile_mem[entry], starting_address[entry], size[entry]);
            mapped[entry] = 0;
            num_mapped_segments--;
        } else {
            int x;
            x = random() % 4;
            size[entry] = (x == 0) ? 1024 * 64 : (x == 1) ? 1024 * 1024 * 2 : (x == 2) ? 1024 * 128 : 1024 * 512;
            attempted_mappings++;
            mapped[entry] = (hn_find_memory(14, size[entry], &tile_mem[entry], &starting_address[entry]) ==
                             HN_SUCCEEDED);
            if (mapped[entry]) {
                num_mapped_segments++;
                successful_mappings++;
                hn_allocate_memory(tile_mem[entry], starting_address[entry], size[entry]);
            }
        }
    }
    printf("successful mappings: %d, attempted mappings: %d\n", successful_mappings, attempted_mappings);
    printf("unmapping rest of segments...\n");
    while (num_mapped_segments) {
        i = random() % 50000;
        if (mapped[i]) {
            hn_release_memory(tile_mem[i], starting_address[i], size[i]);
            //fn_show_tiles_with_memories();
            mapped[i] = 0;
            num_mapped_segments--;
        }
    }
    printf("done\n");

    printf("\n\n");
    num_iterations = 10000;

    printf("Test3: Randomly reserving and releasing cluster, memory and network bandwidth\n");
    printf("       Performing %d iterations\n", num_iterations);
    printf("--------------------------------------------------------------------------------------------------------------\n");
    num_mappings = 0;
    for (i = 0; i < 50000; i++) mapped[i] = 0;

    for (i = 0; i < num_iterations; i++) {
        entry = random() % 50000;
        if (mapped[entry]) {
            switch (request_type[entry]) {
                case 0:
                    hn_release_read_cluster_bandwidth(request_bw[entry]);
                    break;
                case 1:
                    hn_release_write_cluster_bandwidth(request_bw[entry]);
                    break;
                case 2:
                    hn_release_read_memory_bandwidth(mem_tile[entry], request_bw[entry]);
                    break;
                case 3:
                    hn_release_write_memory_bandwidth(mem_tile[entry], request_bw[entry]);
                    break;
                case 4:
                    hn_release_network_bandwidth(src_tile[entry], dst_tile[entry], request_bw[entry]);
                    break;
            }
            num_mappings--;
            mapped[entry] = 0;
        } else {
            x = random() % 5;
            attempted_mappings++;
            switch (x) {
                case 0:
                    hn_get_available_read_cluster_bandwidth(&bw);
                    if (bw > 0) {
                        successful_mappings++;
                        num_mappings++;
                        mapped[entry] = 1;
                        request_bw[entry] = bw / 100;
                        request_type[entry] = 0;
                        hn_reserve_read_cluster_bandwidth(request_bw[entry]);
                    }
                    break;
                case 1:
                    hn_get_available_write_cluster_bandwidth(&bw);
                    if (bw > 0) {
                        successful_mappings++;
                        num_mappings++;
                        mapped[entry] = 1;
                        request_bw[entry] = bw / 100;
                        request_type[entry] = 1;
                        hn_reserve_write_cluster_bandwidth(request_bw[entry]);
                    }
                    break;
                case 2:
                    x = random() % 4;
                    if (x == 0) mem_tile[entry] = 0;
                    else if (x == 1) mem_tile[entry] = 7;
                    else if (x == 2)
                        mem_tile[entry] = 56;
                    else mem_tile[entry] = 63;
                    hn_get_available_read_memory_bandwidth(mem_tile[entry], &bw);
                    if (bw > 0) {
                        successful_mappings++;
                        num_mappings++;
                        mapped[entry] = 1;
                        request_bw[entry] = bw / 100;
                        request_type[entry] = 2;
                        hn_reserve_read_memory_bandwidth(mem_tile[entry], request_bw[entry]);
                    }
                    break;
                case 3:
                    x = random() % 4;
                    if (x == 0) mem_tile[entry] = 0;
                    else if (x == 1) mem_tile[entry] = 7;
                    else if (x == 2)
                        mem_tile[entry] = 56;
                    else mem_tile[entry] = 63;
                    hn_get_available_write_memory_bandwidth(mem_tile[entry], &bw);
                    if (bw > 0) {
                        successful_mappings++;
                        num_mappings++;
                        mapped[entry] = 1;
                        request_bw[entry] = bw / 100;
                        request_type[entry] = 3;
                        hn_reserve_write_memory_bandwidth(mem_tile[entry], request_bw[entry]);
                    }
                    break;
                case 4:
                    src_tile[entry] = random() % num_tiles;
                    dst_tile[entry] = random() % num_tiles;
                    hn_get_available_network_bandwidth(src_tile[entry], dst_tile[entry], &bw);
                    if (bw > 0) {
                        successful_mappings++;
                        num_mappings++;
                        mapped[entry] = 1;
                        request_bw[entry] = bw / 100;
                        request_type[entry] = 4;
                        hn_reserve_network_bandwidth(src_tile[entry], dst_tile[entry], request_bw[entry]);
                    }
                    break;
            }
        }
    }

    printf("successful mappings: %d, attempted mappings: %d\n", successful_mappings, attempted_mappings);

    printf("releasing mappings...\n");
    for (i = 0; i < 50000; i++) {
        if (mapped[i]) {
            switch (request_type[i]) {
                case 0:
                    hn_release_read_cluster_bandwidth(request_bw[i]);
                    break;
                case 1:
                    hn_release_write_cluster_bandwidth(request_bw[i]);
                    break;
                case 2:
                    hn_release_read_memory_bandwidth(mem_tile[i], request_bw[i]);
                    break;
                case 3:
                    hn_release_write_memory_bandwidth(mem_tile[i], request_bw[i]);
                    break;
                case 4:
                    hn_release_network_bandwidth(src_tile[i], dst_tile[i], request_bw[i]);
                    break;
            }
        }
    }
    printf("done\n");

    printf("\n\n");
    num_iterations = 1000;
#ifdef VERBOSE
    num_iterations = 15;
#endif
    printf("Test4: Tiles assignment and releasing (one random set each iteration)\n");
    printf("       Performing %d iterations\n", num_iterations);
    printf("--------------------------------------------------------------------------------------------------------------\n");
    num_mappings = 0;
    successful_mappings = 0;
    attempted_mappings = 0;
    unsigned long long size_mem = 1024 * 1024;

//    unsigned int mapped[50000];
    for (i = 0; i < 50000; i++) mapped[i] = 0;

    for (i = 0; i < 64; i++) types[i] = HN_TILE_FAMILY_NONE;
    // We ask for random number of tiles but always the same type of unit
    for (i = 0; i < num_iterations; i++) {
        x = random() % 20;
        if (mapped[x]) {
            hn_release_units_set(request_num_tiles[x], tiles_dst_ptr[x]);
#ifdef VERBOSE
            printf("%2d tiles released : ", request_num_tiles[x]);
            for (int t = 0; t < request_num_tiles[x]; t++)
                printf("%2d, ", tiles_dst_ptr[x][t]);
            printf("\n");
#endif
            num_mappings--;
            mapped[x] = 0;
        } else {
            request_num_tiles[x] = random() % 10;
            if (request_num_tiles[x] == 0) request_num_tiles[x] = 1;
            attempted_mappings++;
#ifdef VERBOSE
            printf("\n try to find %2d tiles \n ", request_num_tiles[x]);
#endif
            if (hn_find_units_set(36, request_num_tiles[x], types, tiles_dst_ptr[x], types_dst_ptr[x]) ==
                HN_SUCCEEDED) {
                hn_reserve_units_set(request_num_tiles[x], tiles_dst_ptr[x]);
                mapped[x] = 1;
                successful_mappings++;
                num_mappings++;
#ifdef VERBOSE
                printf("%2d tiles reserved : ", request_num_tiles[x]);
                for (int t = 0; t < request_num_tiles[x]; t++)
                    printf("%2d, ", tiles_dst_ptr[x][t]);
                printf("\nmemory tiles: ");
#endif

                for (int t = 0; t < request_num_tiles[x]; t++) {
                    uint32_t tile_mem, st_address;
                    auto err = hn_find_memory(tiles_dst_ptr[x][t], size_mem, &tile_mem, &st_address);
#ifdef VERBOSE
                    printf("%2d, ", tile_mem);
#endif
                }
#ifdef VERBOSE
                printf("\n");
#endif
            } else {
#ifdef VERBOSE
                printf("%2d tiles not found \n", request_num_tiles[x]);
#endif
            }

        }
    }
    printf("successful mappings: %d, attempted mappings: %d\n", successful_mappings, attempted_mappings);


    printf("releasing all mappings...\n");
    for (i = 0; i < 20; i++) {
        if (mapped[i]) {
            hn_release_units_set(request_num_tiles[i], tiles_dst_ptr[i]);
            num_mappings--;
            mapped[i] = 0;
        }
    }

    printf("\n\n");
    num_iterations = 1000;
#ifdef VERBOSE
    num_iterations = 10;
#endif
    printf("Test5: Tiles sets (one request asks for as many sets as possible) assignment and releasing\n");
    printf("       Performing %d iterations\n", num_iterations);
    printf("--------------------------------------------------------------------------------------------------------------\n");
    num_mappings = 0;
    successful_mappings = 0;
    attempted_mappings = 0;
    for (i=0;i<64;i++) types[i] = HN_TILE_FAMILY_NONE;
    // We ask for random number of tiles but always the same type of unit
    uint32_t nt;
    uint32_t **p1 = NULL;
    uint32_t **p2 = NULL;
    uint32_t num;
    for (i=0;i<num_iterations;i++) {
        nt = random() % 16;
        if (nt == 0) nt = 1;
        attempted_mappings++;
#ifdef VERBOSE
        printf("\n try to find %2d tiles \n ", nt);
#endif
        if ( hn_find_units_sets(36, nt, types, &p1, &p2, &num) == HN_SUCCEEDED ) {
#ifdef VERBOSE
            printf("iteration %d: num tiles per set: %d, num sets found: %d\n", i, nt, num);

            for (x=0;x<num;x++) {
                printf("set %4d : ", x);
                for (y=0;y<nt;y++) printf("%2d ", p1[x][y]);
                printf("\n");
            }
#endif
            for (x=0;x<num;x++) hn_reserve_units_set(nt, p1[x]);
            successful_mappings++;
            for (x=0;x<num;x++) hn_release_units_set(nt, p1[x]);
            // we free memory resources
            for (x=0;x<num;x++) {free(p1[x]); free(p2[x]);}
            free(p1);
            free(p2);
        } else {
#ifdef VERBOSE
            printf("iteration %d, no sets found\n", i);
#endif
        }
    }
    printf("successful mappings: %d, attempted mappings: %d\n", successful_mappings, attempted_mappings);



    printf("\n\n");
    printf("tests finished\n");

    hn_end();
    return 0;
}