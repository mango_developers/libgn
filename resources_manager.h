#ifndef RESOURCES_MANAGER_H
#define RESOURCES_MANAGER_H


#include <iostream>
#include <fstream>
#include "hn_include/hn_resource_manager.h"
#include "hn.h"
#include "rapidxml.hpp"
#include <rapidxml_print.hpp>
#include "regex"
#include "logger.h"


#define MANGO_ROOT            "/opt/mango"
#define MANGO_CONFIG_XML     MANGO_ROOT "/usr/local/share/config.xml"

#define NUM_TILES_X 5
#define NUM_TILES_Y 4
#define MEM_SIZE_MB ( 1024UL )
#define BW  40 * 64 / 8
#define BW_MEM  40 * 64
#define BW_CL  1024

#define MBinBYTES(x) ((x)*1024UL*1024UL)

#define HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE    35

namespace gnemu {

    class ResourcesManager {
    public:

        static ResourcesManager *instance();

        ResourcesManager(ResourcesManager const &) = delete;

        void operator=(ResourcesManager const &)  = delete;

        ~ResourcesManager();

        uint32_t fill_default_config();

        uint32_t fill_config();

        uint32_t get_num_tiles(uint32_t *num_tiles, uint32_t *num_tiles_x, uint32_t *num_tiles_y);

        const hn_rscmgt_info_t *get_info() { return &configuration; }

        uint32_t get_tile_info(uint32_t tile, hn_rscmgt_tile_info_t *data);

        uint32_t get_memory_size(uint32_t tile, uint32_t *size);

        uint32_t get_memory_size(unsigned long long *size);

        uint32_t
        get_memory(uint32_t tile, uint32_t *size, uint32_t *free, uint32_t *starting_addr);

        uint32_t get_free_memory(uint32_t tile, uint32_t size, uint32_t *starting_addr);

        uint32_t is_tile_assigned(uint32_t tile, uint32_t *avail);

        uint32_t set_tile_assigned(uint32_t tile);

        uint32_t set_tile_avail(uint32_t tile);

        uint32_t get_available_read_memory_bw(uint32_t tile, unsigned long long *bw);

        uint32_t get_available_write_memory_bw(uint32_t tile, unsigned long long *bw);

        uint32_t get_available_network_bw(uint32_t tile_src, uint32_t tile_dst, unsigned long long *bw);

        uint32_t get_available_router_bw(uint32_t tile, uint32_t port, unsigned long long *bw);

        uint32_t reserve_read_memory_bw(uint32_t tile, unsigned long long bw);

        uint32_t reserve_write_memory_bw(uint32_t tile, unsigned long long bw);

        uint32_t reserve_router_bw(uint32_t tile, uint32_t port, unsigned long long bw);

        uint32_t reserve_network_bw(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw);

        uint32_t release_read_memory_bw(uint32_t tile, unsigned long long bw);

        uint32_t release_write_memory_bw(uint32_t tile, unsigned long long bw);

        uint32_t release_router_bw(uint32_t tile, uint32_t port, unsigned long long bw);

        uint32_t release_network_bw(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw);

        uint32_t get_available_read_cluster_bw(unsigned long long *bw);

        uint32_t get_available_write_cluster_bw(unsigned long long *bw);

        uint32_t reserve_read_cluster_bw(unsigned long long bw);

        uint32_t reserve_write_cluster_bw(unsigned long long bw);

        uint32_t release_read_cluster_bw(unsigned long long bw);

        uint32_t release_write_cluster_bw(unsigned long long bw);

        uint32_t
        find_memory(uint32_t tile, uint32_t size, uint32_t *tile_mem, uint32_t *starting_addr);

        uint32_t allocate_memory(uint32_t tile, uint32_t addr, uint32_t size);

        uint32_t release_memory(uint32_t tile, uint32_t addr, uint32_t size);

        uint32_t get_network_distance(uint32_t tile_src, uint32_t tile_dst, uint32_t *dst);

        uint32_t find_units_set(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t **tiles_dst,
                                   uint32_t *types_dst);

        uint32_t find_units_sets(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t ***tiles_dst,
                                        uint32_t ***types_dst, uint32_t *num);

        uint32_t reserve_units_set(uint32_t num_tiles, uint32_t *tiles);

        uint32_t release_units_set(uint32_t num_tiles, uint32_t *tiles);


    private:

        ResourcesManager();

        hn_rscmgt_info_st configuration;

        uint32_t checkMemory(uint32_t tile, uint32_t size, uint32_t *tile_mem, uint32_t *starting_addr);

        uint32_t find_single_units_set(uint32_t tile, uint32_t num_tiles, uint32_t *types, uint32_t **tiles_dst,
                              uint32_t *dst);

        uint32_t checkType(uint32_t tile_cur, uint32_t *num_tiles_cur, uint32_t num_tiles, uint32_t *tiles_cur,
                           const uint32_t *types);

    };
}


#endif //RESOURCES_MANAGER_H

