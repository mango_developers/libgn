#include "resources_manager.h"
#include <fcntl.h>
#include <assert.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>

#define MANGO_SEMAPHORE       "/mango_sem"
#define MANGO_DEVICE_MEMORY   "/tmp/device_memory.dat"
#define MANGO_ROOT            "/opt/mango"
#define MANGO_CONFIG_XML     MANGO_ROOT "/usr/local/share/config.xml"

#define NUM_VNS HN_MAX_VNS

#define UNUSED(x) (void)x


std::string GN_RETURN_ROOT = MANGO_ROOT "/bin/gn_return";

uint32_t event_register_offset = 0;

static uint32_t *mem;
sem_t * sem_id;
static int f_mem;
static void init_semaphore(void) {

    mode_t old_mask = umask(0);

    sem_id = sem_open(MANGO_SEMAPHORE, O_CREAT, 0666, 1);
    if (sem_id == SEM_FAILED) {
        perror("Error opening semaphore " MANGO_SEMAPHORE);
        sem_id = NULL;
    }

    umask(old_mask);

}

uint32_t hn_initialize(hn_filter_t filter, uint32_t partition_strategy, uint32_t socket_connection, uint32_t reset, uint32_t capture_signals_enable){

    UNUSED(filter);
    UNUSED(partition_strategy);
    UNUSED(socket_connection);
    UNUSED(reset);
    UNUSED(capture_signals_enable);


    init_semaphore();
    if (sem_id == NULL) {
        return HN_SYNCH_RESOURCE_DOES_NOT_EXIST;
    }

    mode_t old_mask = umask(0);
    f_mem = open(MANGO_DEVICE_MEMORY,O_RDWR | O_CREAT, 0666);
    if (f_mem==-1) {
        perror("Unable to read device memory " MANGO_DEVICE_MEMORY);
        return HN_MEMORY_NOT_PRESENT_IN_TILE;
    }



    umask(old_mask);


    unsigned long long mem_size;
    gnemu::ResourcesManager::instance()->get_memory_size(&mem_size);

    assert(0 == ftruncate(f_mem, mem_size));

    mem = (uint32_t*) mmap (NULL, mem_size,
                            PROT_READ|PROT_WRITE, MAP_SHARED, f_mem,0);

    if (mem==MAP_FAILED) {
        return HN_MEMORY_NOT_PRESENT_IN_TILE;
    }

    return HN_SUCCEEDED;
}

uint32_t hn_reset(uint32_t tile) {

    // This is called by barbeque. In order to be sure that the semaphore is cleaned
    // just close it and delete it. Ugly hack but ok for GN emulation

    sem_close(sem_id);
    sem_unlink(MANGO_SEMAPHORE);

    init_semaphore();
    if (sem_id == NULL) {
        return HN_SYNCH_RESOURCE_DOES_NOT_EXIST;
    }

    // Reserve space for sync registers
//    next_address = sizeof(uint32_t) * 32;

    UNUSED(tile);
    hn_allocate_memory(0,0,sizeof(uint32_t) * 32);
    return HN_SUCCEEDED;
}

uint32_t hn_end() {

    sem_close(sem_id);
    close(f_mem);

    return HN_SUCCEEDED;
}

uint32_t hn_set_tlb(uint32_t tile, uint32_t entry, uint32_t vaddr_ini, uint32_t vaddr_end,
                    uint32_t paddr, uint32_t mem_rsc, uint32_t tilereg_rsc, uint32_t reg_rsc, uint32_t tile_rsc)
{
    UNUSED(tile);
    UNUSED(entry);
    UNUSED(vaddr_ini);
    UNUSED(vaddr_end);
    UNUSED(paddr);
    UNUSED(mem_rsc);
    UNUSED(tilereg_rsc);
    UNUSED(reg_rsc);
    UNUSED(tile_rsc);
    return HN_SUCCEEDED;
}

uint32_t hn_get_num_tiles(uint32_t *num_tiles, uint32_t *num_tiles_x, uint32_t *num_tiles_y) {
    auto info = gnemu::ResourcesManager::instance()->get_info();
    *num_tiles = info->num_tiles;
    *num_tiles_x = info->num_cols;
    *num_tiles_y = info->num_rows;
    return HN_SUCCEEDED;
}

uint32_t hn_get_num_vns(uint32_t *num_vns) {
    *num_vns = NUM_VNS;
    return HN_SUCCEEDED;
}

uint32_t hn_boot_unit(uint32_t tile, uint32_t tile_memory, uint32_t addr, const char *protocol_img_path, const char *kernel_img_path) {
    UNUSED(tile);
    UNUSED(tile_memory);
    UNUSED(addr);
    UNUSED(protocol_img_path);
    UNUSED(kernel_img_path);
    return HN_SUCCEEDED;
}



uint32_t hn_get_tile_info(uint32_t tile, hn_tile_info_t *data){

    auto info = gnemu::ResourcesManager::instance()->get_info();
    if (tile > info->num_tiles)
        return HN_TILE_DOES_NOT_EXIST;
    data->memory_attached = info->tile_info[tile].memory_size;
    data->unit_model = info->tile_info[tile].subtype;
    data->unit_family = info->tile_info[tile].type;

    return HN_SUCCEEDED;

}
uint32_t hn_get_memory_size(uint32_t tile, uint32_t *size) {

    uint32_t mem_size = 0;
    auto status = gnemu::ResourcesManager::instance()->get_memory_size(tile, &mem_size);
    *size = mem_size;
    return HN_SUCCEEDED;
}


uint32_t hn_get_memory_size(unsigned long long *size) {

    unsigned long long  mem_size = 0;
    auto status = gnemu::ResourcesManager::instance()->get_memory_size(&mem_size);
    *size = mem_size;
    return HN_SUCCEEDED;
}


uint32_t hn_write_image_into_memory(const char *file_name, uint32_t tile, uint32_t addr) {
    UNUSED(file_name);
    UNUSED(tile);
    UNUSED(addr);
    return HN_SUCCEEDED;
}

uint32_t hn_run_kernel(uint32_t tile, uint32_t addr, char *args) {
    pid_t pid;

    printf("ARG STRING: %s\n", args);

    pid = fork();
    if (!pid){ /* child, executor */
        printf("system(%s);\n", args);
        int ret = system(args);

        char n[11];
        execl(MANGO_ROOT "/bin/gn_return", n, NULL);
        printf("***\n!!!\nEXECL ERROR\n!!!\n***\n");
        exit(0);
    }

    return HN_SUCCEEDED;
}




uint32_t hn_read_register(uint32_t tile, uint32_t reg, uint32_t *data) {

    reg /= 4;

    UNUSED(tile);

    sem_wait(sem_id);

    *data=mem[reg];
    mem[reg]=0;

    sem_post(sem_id);

    return HN_SUCCEEDED;
}

uint32_t hn_write_register(uint32_t tile, uint32_t reg, uint32_t data) {

    UNUSED(tile);

    sem_wait(sem_id);

    reg /= 4;

    if (reg % 8 != 0) {
        mem[reg] += data;
    } else {
        mem[reg] = data;
    }

    sem_post(sem_id);

    return HN_SUCCEEDED;
}
uint32_t hn_read_synch_register(uint32_t reg, uint32_t *data) {

    reg /= 4;

    sem_wait(sem_id);

    *data=mem[reg];
    mem[reg]=0;

    sem_post(sem_id);

    return HN_SUCCEEDED;
}

uint32_t hn_write_synch_register(uint32_t reg, uint32_t data) {

    reg /= 4;

    sem_wait(sem_id);

    if (reg % 8 != 0) {
        mem[reg] += data;
    } else {
        mem[reg] = data;
    }

    sem_post(sem_id);

    return HN_SUCCEEDED;
}

uint32_t hn_post(uint32_t addr, uint32_t value)
{

    hn_write_register(0, addr, value);

    return HN_SUCCEEDED;
}

uint32_t hn_wait(uint32_t addr, uint32_t value)
{
    uint32_t v, acc_value=0;

    do {
        hn_read_register(0, addr, &v);
        if (addr % 8 != 0) {
            acc_value += v;
        }
    } while (acc_value != value);

    return HN_SUCCEEDED;
}

uint32_t hn_write_memory(uint32_t tile, uint32_t addr, uint32_t size, const char *data) {

    UNUSED(tile);

    memcpy(mem + (addr/4), data, size);

    return HN_SUCCEEDED;
}

uint32_t hn_read_memory(uint32_t tile, uint32_t addr, uint32_t size, char *data) {

    UNUSED(tile);

    memcpy(data, mem + (addr/ 4), size);

    return HN_SUCCEEDED;
}

uint32_t hn_get_synch_id ( uint32_t *ID, uint32_t target_tile, uint32_t type ) {
    UNUSED(target_tile);

    // Addresses multiple of 8 are dedicated to non-additive registers
    // Addresses multiple of 4 but not of 8 are dedicated to additive registers

    if ( type == HN_READRESET_REG_TYPE ) {
        if ( event_register_offset % 8 == 0 ) {
            event_register_offset += 8;
        } else {
            event_register_offset += 4;	// We lost a register here, but who cares
        }
    }

    if ( type == HN_READRESET_INCRWRITE_REG_TYPE ) {
        if ( event_register_offset % 4 == 0 && event_register_offset % 8 != 0 ) {
            event_register_offset += 8;
        } else {
            event_register_offset += 4;	// We lost a register here, but who cares
        }
    }

    *ID = event_register_offset;

    return HN_SUCCEEDED;
}

uint32_t hn_release_synch_id ( uint32_t ID) {
    // Not implemented, it's not a big problem in GN to deallocate events
    return HN_SUCCEEDED;
}

uint32_t hn_get_synch_id_array ( uint32_t *ID, uint32_t n, uint32_t target_tile, uint32_t type ) {
    UNUSED(target_tile);
    int i;

    for(i=0; i<n; i++) {
        hn_get_synch_id(&ID[i], target_tile, type);
    }
    return HN_SUCCEEDED;
}


/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_tile_temperature( uint32_t tile, float *temp ) {
    UNUSED(tile);
    *temp = 0;
    return HN_SUCCEEDED;
}



//
//*************************************************************
//                      NEW API
//*************************************************************
/*!
 * \brief Gets the info structure
 *
 * \return a constant data pointer to the internal structure
 */
const hn_rscmgt_info_t *hn_get_info(){
    return gnemu::ResourcesManager::instance()->get_info();
}

uint32_t size_aligment (uint32_t size){
    return (size == 0) ? 64 : size + ((((size + 63)/64)*64) - size);
}

uint32_t size_aligment (unsigned long long size){
    return (size == 0) ? 64 : size + ((((size + 63)/64)*64) - size);
}
/*
 * \brief This function delivers information about the closest memory (to a tile) with the available memory size
 */

uint32_t hn_find_memory(uint32_t tile, unsigned long long size, uint32_t *tile_mem, uint32_t *starting_addr){


    uint32_t size_aligned = size_aligment(size);

    auto err =  gnemu::ResourcesManager::instance()->find_memory(tile, size_aligned, tile_mem, starting_addr);

    return err;
}

/*
 * \brief This function delivers information about the memories with the available memory size
 */
uint32_t hn_find_memories(uint32_t size, uint32_t *tiles_mem, uint32_t *starting_addr, uint32_t *num_memories){


    uint32_t size_aligned  = size_aligment(size);

    uint32_t tile_cur = 0, addr_cur = 0, num_cur=0;
    uint32_t num_tiles, num_tiles_x, num_tiles_y;

    auto err = gnemu::ResourcesManager::instance()->get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);

    if (err != HN_SUCCEEDED){
        return err;
    }

    for (tile_cur = 0; tile_cur<num_tiles; tile_cur++){
        err = gnemu::ResourcesManager::instance()->get_free_memory(tile_cur, size_aligned, &addr_cur);
        if (err == HN_SUCCEEDED){
            num_cur++;
            tiles_mem = (uint32_t*) realloc (tiles_mem, num_cur * sizeof(uint32_t));
            starting_addr = (uint32_t*) realloc (starting_addr, num_cur * sizeof(uint32_t));
            if (tiles_mem!= nullptr && starting_addr!= nullptr) {
                tiles_mem [num_cur-1] = tile_cur;
                starting_addr [num_cur-1] = addr_cur;
            }
        }
    }

    if (num_cur > 0) {
        *num_memories = num_cur;
        return HN_SUCCEEDED;
    } else {
        *num_memories = 0;
        return HN_NOT_ENOUGH_MEMORY_AVAILABLE;
    }

}

/*
 * \brief This function allocates a memory segment
 */
uint32_t hn_allocate_memory(uint32_t tile, uint32_t addr, uint32_t size){


    uint32_t size_aligned = size_aligment(size);

    auto err =  gnemu::ResourcesManager::instance()->allocate_memory(tile, addr, size_aligned);
    return err;
}

/*
 * \brief This function releases a memory segment
 */
uint32_t hn_release_memory(uint32_t tile, uint32_t addr, uint32_t size){

    uint32_t size_aligned = size_aligment(size);

    auto err =  gnemu::ResourcesManager::instance()->release_memory(tile, addr, size_aligned);
    return err;
}

/*
 * \brief This function delivers the available network bandwidth between two tiles
 */
uint32_t hn_get_available_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long *bw){

    return gnemu::ResourcesManager::instance()->get_available_network_bw(tile_src, tile_dst, bw);
}

/*
 * \brief This function delivers the available read memory bandwidth in a tile
 */
uint32_t hn_get_available_read_memory_bandwidth(uint32_t tile, unsigned long long *bw){

    return gnemu::ResourcesManager::instance()->get_available_read_memory_bw(tile, bw);
}

/*
 * \brief This function delivers the available write memory bandwidth in a tile
 */
uint32_t hn_get_available_write_memory_bandwidth(uint32_t tile, unsigned long long *bw){

    return gnemu::ResourcesManager::instance()->get_available_write_memory_bw(tile, bw);
}

/*
 * \brief This function delivers the available read cluster bandwidth
 */
uint32_t hn_get_available_read_cluster_bandwidth(unsigned long long *bw){

    return gnemu::ResourcesManager::instance()->get_available_read_cluster_bw(bw);
}

/*
 * \brief This function delivers the available write cluster bandwidth
 */uint32_t hn_get_available_write_cluster_bandwidth(unsigned long long *bw){

    return gnemu::ResourcesManager::instance()->get_available_write_cluster_bw(bw);
}

/*
 * \brief This function reserves network bandwidth between two tiles
 */
uint32_t hn_reserve_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->reserve_network_bw(tile_src, tile_dst, bw);
    return err;
}

/*
 * \brief This function reserves read memory bandwidth in a tile
 */
uint32_t hn_reserve_read_memory_bandwidth(uint32_t tile, unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->reserve_read_memory_bw(tile,bw);
    return err;
}

/*
 * \brief This function reserves write memory bandwidth in a tile
 */
uint32_t hn_reserve_write_memory_bandwidth(uint32_t tile, unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->reserve_write_memory_bw(tile,bw);
    return err;
}

/*
 * \brief This function reserves read cluster bandwidth
 */
uint32_t hn_reserve_read_cluster_bandwidth(unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->reserve_read_cluster_bw(bw);
    return err;
}

/*
 * \brief This function reserves write cluster bandwidth
 */
uint32_t hn_reserve_write_cluster_bandwidth(unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->reserve_write_cluster_bw(bw);
    return err;
}

/*
 * \brief This function releases network bandwidth between two tiles
 */
uint32_t hn_release_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw){
    auto err = gnemu::ResourcesManager::instance()->release_network_bw(tile_src, tile_dst, bw);
    return err;
}

/*
 * \brief This function releases read memory bandwidth in a tile
 */
uint32_t hn_release_read_memory_bandwidth(uint32_t tile, unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->release_read_memory_bw(tile, bw);
    return err;
}

/*
 * \brief This function releases write memory bandwidth in a tile
 */
uint32_t hn_release_write_memory_bandwidth(uint32_t tile, unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->release_write_memory_bw(tile, bw);
    return err;
}

/*
 * \brief This function releases read cluster bandwidth
 */
uint32_t hn_release_read_cluster_bandwidth(unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->release_read_cluster_bw(bw);
    return err;
}

/*
 * \brief This function releases write cluster bandwidth
 */
uint32_t hn_release_write_cluster_bandwidth(unsigned long long bw){

    auto err = gnemu::ResourcesManager::instance()->release_write_cluster_bw(bw);
    return err;
}

/*
 * \brief This function returns a set of tiles according to the requested types and number of tiles
 */

uint32_t hn_find_units_set(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t *tiles_dst, uint32_t *types_dst){
    uint32_t *tiles = nullptr;
    auto err =  gnemu::ResourcesManager::instance()->find_units_set(tile, num_tiles, types, &tiles, types_dst);

    if (err != HN_SUCCEEDED )
        return err;

    for (int i = 0; i< num_tiles; i++){
        tiles_dst[i] = tiles[i];
    }
    free (tiles);
    return err;
}

/*
 * \brief This function returns all possible (disjoint) set of tiles according to the request types and number of tiles
 */
uint32_t hn_find_units_sets(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t ***tiles_dst, uint32_t ***types_dst, uint32_t *num){
    auto err = gnemu::ResourcesManager::instance()->find_units_sets(tile, num_tiles, types, tiles_dst, types_dst, num);
    return err;
}

/*
 * \brief This function reserves a set of tiles
 */
uint32_t hn_reserve_units_set(uint32_t num_tiles, uint32_t *tiles){
    gnemu::ResourcesManager::instance()->reserve_units_set(num_tiles, tiles);
    return HN_SUCCEEDED;

}

/*
 * \brief This function releases a set of tiles
 */
uint32_t hn_release_units_set(uint32_t num_tiles, uint32_t *tiles){
    gnemu::ResourcesManager::instance()->release_units_set(num_tiles, tiles);
    return  HN_SUCCEEDED;
}


/**************************************************************************************************
***************************************************************************************************
**/
const char *hn_to_str_unit_family(uint32_t family) {
    switch(family) {
        case HN_TILE_FAMILY_PEAK     : return "PEAK";
        case HN_TILE_FAMILY_NUPLUS   : return "NUPLUS";
        case HN_TILE_FAMILY_DCT      : return "DCT";
        case HN_TILE_FAMILY_TETRAPOD : return "TETRAPOD";
        case HN_TILE_FAMILY_GN       : return "GN";
        default                      : return "UNKNOWN";
    }
}

const char *hn_to_str_unit_model(uint32_t model) {
    const char *str_model;
    if ((str_model = hn_peak_to_str_unit_model(model)) != NULL)
        return str_model;
    else if ((str_model = hn_nuplus_to_str_unit_model(model)) != NULL)
        return str_model;
    else if ((str_model = hn_dct_to_str_unit_model(model)) != NULL)
        return str_model;
    else if ((str_model = hn_tetrapod_to_str_unit_model(model)) != NULL)
        return str_model;

    return "UNKNOWN";
}

const char *hn_peak_to_str_unit_model(uint32_t model) {
    switch(model) {
        case HN_PEAK_MANYCORE_0 : return "PMC2";
        case HN_PEAK_MANYCORE_1 : return "PMC4";
        case HN_PEAK_MANYCORE_2 : return "PMC8";
        case HN_PEAK_MANYCORE_3 : return "PMC8";
        case HN_PEAK_MANYCORE_4 : return "PMC8";
        case HN_PEAK_MANYCORE_5 : return "PMC8";
        case HN_PEAK_MANYCORE_6 : return "PMC8";
        default                  : return NULL;
    }
}


const char *hn_dct_to_str_unit_model(uint32_t model) {
    switch (model) {
        case HN_DCT_MODEL_BASE         :
            return "DCT0";
        default                        :
            return NULL;
    }
}

const char *hn_nuplus_to_str_unit_model(uint32_t model) {
    switch(model) {
        case HN_NUPLUS_MODEL_BASE      : return "NU+B";
        default                        : return NULL;
    }
}

const char *hn_tetrapod_to_str_unit_model(uint32_t model) {
    switch(model) {
        case HN_TETRAPOD_MODEL_DE4WI4  : return "TTDE4WI4";
        default                        : return NULL;
    }
}
