#include "logger.h"
#include <cstdlib>

std::string conf_file = "/etc/libgn.conf";

INITIALIZE_EASYLOGGINGPP

namespace gnemu {

    el::Logger* gnemu_log = el::Loggers::getLogger("default");

    extern void gnemu_init_logger() {
            char * mango_path = getenv("MANGO_ROOT");
            std::string conf_file;
            if (mango_path) {
				conf_file.assign(mango_path);
				conf_file += "/etc/libgn.conf";
			}

            el::Configurations conf(conf_file);
            // Reconfigure
            el::Loggers::reconfigureLogger("default", conf);

            gnemu_log->info ("gnemu_init_logger: configuration file: %v", conf_file);

    }

}
