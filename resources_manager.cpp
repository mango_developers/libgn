#include "resources_manager.h"


using namespace rapidxml;

namespace gnemu {
    ResourcesManager *ResourcesManager::instance() {

        static ResourcesManager _instance;
        return &_instance;
    };

    ResourcesManager::ResourcesManager() {
        gnemu::gnemu_init_logger();
        fill_config();
    };

    ResourcesManager::~ResourcesManager() {
        for (auto i = 0; i < configuration.num_tiles; i++) {
            if (configuration.tile_info[i].memory_size > 0) {
                auto del = configuration.tile_info[i].first_memory_slot;
                while (del != nullptr) {
                    auto tmp = del->next_memory_slot;
                    free(del);
                    del = (hn_rscmgt_memory_slot_t *) tmp;
                }

            }
        }

    };

    uint32_t ResourcesManager::fill_default_config() {
        unsigned long long last_addr = 0;
        configuration.arch_id = 0;
        configuration.num_tiles = NUM_TILES_X * NUM_TILES_Y;
        configuration.num_cols = NUM_TILES_X;
        configuration.num_rows = NUM_TILES_Y;
        configuration.read_cluster_bw = MBinBYTES(BW_CL);
        configuration.avail_read_cluster_bw = MBinBYTES(BW_CL);
        configuration.write_cluster_bw = MBinBYTES(BW_CL);
        configuration.avail_write_cluster_bw = MBinBYTES(BW_CL);

        for (auto i = 0; i < configuration.num_tiles; i++) {
            configuration.tile_info[i].type = HN_TILE_FAMILY_GN;
            configuration.tile_info[i].subtype = HN_TILE_MODEL_NONE;
            configuration.tile_info[i].assigned = 0;
            configuration.tile_info[i].preassigned = 0;
            if ((i == 0) || (i == (NUM_TILES_X - 1)) || (i == (NUM_TILES_X * (NUM_TILES_Y - 1))) ||
                (i == (NUM_TILES_X * NUM_TILES_Y - 1))) {
                configuration.tile_info[i].memory_size = MBinBYTES(MEM_SIZE_MB);
                configuration.tile_info[i].free_memory = MBinBYTES(MEM_SIZE_MB);
                configuration.tile_info[i].read_memory_bw = MBinBYTES(BW_MEM);
                configuration.tile_info[i].avail_read_memory_bw = MBinBYTES(BW_MEM);
                configuration.tile_info[i].write_memory_bw = MBinBYTES(BW_MEM);
                configuration.tile_info[i].avail_write_memory_bw = MBinBYTES(BW_MEM);
                configuration.tile_info[i].first_memory_slot = (hn_rscmgt_memory_slot_t *) malloc(
                        sizeof(hn_rscmgt_memory_slot_t));
                configuration.tile_info[i].first_memory_slot->size = MBinBYTES(MEM_SIZE_MB);
                configuration.tile_info[i].first_memory_slot->start_address = last_addr;
                last_addr += MBinBYTES(MEM_SIZE_MB);
                configuration.tile_info[i].first_memory_slot->end_address = last_addr - 1;
                configuration.tile_info[i].first_memory_slot->next_memory_slot = nullptr;
            } else {
                configuration.tile_info[i].memory_size = 0;
                configuration.tile_info[i].free_memory = 0;
                configuration.tile_info[i].read_memory_bw = 0;
                configuration.tile_info[i].avail_read_memory_bw = 0;
                configuration.tile_info[i].write_memory_bw = 0;
                configuration.tile_info[i].avail_write_memory_bw = 0;
                configuration.tile_info[i].first_memory_slot = nullptr;
            }

            configuration.tile_info[i].north_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].avail_north_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].west_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].avail_west_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].east_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].avail_east_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].south_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].avail_south_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].local_port_bw = MBinBYTES(BW);
            configuration.tile_info[i].avail_local_port_bw = MBinBYTES(BW);

        }

    }


    uint32_t str_to_uint(const char *str) {

        std::string::size_type sz;
        if (strlen(str) != 0) {
            return (uint32_t) std::stoul(str, &sz);
        }
        return 0;
    }

    unsigned long long str_to_ull(const char *str) {

        std::string::size_type sz;
        if (strlen(str) != 0) {
            return std::stoull(str, &sz);
        }
        return 0;
    }

    uint32_t ResourcesManager::fill_config() {

        xml_document<> doc;

        std::ifstream file(MANGO_CONFIG_XML);
        if (!file) {
            gnemu_log->info("Config.xml does not exist. Fill default configuration");
            fill_default_config();
            return 0;
        }

        uint32_t last_addr = 0;

        std::stringstream buffer;
        buffer << file.rdbuf();

        std::string content(buffer.str());
        doc.parse<0>(&content[0]);

        xml_node<> *cur_node = doc.first_node("system");
        std::string arch_id = cur_node->first_attribute("arch_id")->value();
        configuration.arch_id = str_to_uint(arch_id.c_str());

        std::string read_cluster_bw = cur_node->first_attribute("read_cluster_bw")->value();
        configuration.read_cluster_bw = MBinBYTES(str_to_ull(read_cluster_bw.c_str()));

        std::string write_cluster_bw = cur_node->first_attribute("write_cluster_bw")->value();
        configuration.write_cluster_bw = MBinBYTES(str_to_ull(write_cluster_bw.c_str()));

        std::string num_rows = cur_node->first_attribute("num_rows")->value();
        configuration.num_rows = str_to_uint(num_rows.c_str());

        std::string num_cols = cur_node->first_attribute("num_cols")->value();
        configuration.num_cols = str_to_uint(num_cols.c_str());

        configuration.num_tiles = configuration.num_rows * configuration.num_cols;

        uint32_t tile = 0;
        for (xml_node<> *child = cur_node->first_node(); child; child = child->next_sibling(), tile++) {
            configuration.tile_info[tile].type = tile;

            std::string type = child->first_attribute("family")->value();
            configuration.tile_info[tile].type = str_to_uint(type.c_str());

            configuration.tile_info[tile].assigned = configuration.tile_info[tile].preassigned = 0;

            std::string subtype = child->first_attribute("model")->value();
            configuration.tile_info[tile].subtype = str_to_uint(subtype.c_str());

            std::string mem_size = child->first_attribute("mem_size")->value();
            configuration.tile_info[tile].memory_size = configuration.tile_info[tile].free_memory
                    = MBinBYTES(str_to_uint(mem_size.c_str()));

            std::string read_mem_bw = child->first_attribute("read_mem_bw")->value();
            configuration.tile_info[tile].read_memory_bw = configuration.tile_info[tile].avail_read_memory_bw
                    = MBinBYTES(str_to_ull(read_mem_bw.c_str()));

            std::string write_mem_bw = child->first_attribute("write_mem_bw")->value();
            configuration.tile_info[tile].write_memory_bw = configuration.tile_info[tile].avail_write_memory_bw
                    = MBinBYTES(str_to_ull(write_mem_bw.c_str()));

            std::string north_port_bw = child->first_attribute("north_port_bw")->value();
            configuration.tile_info[tile].north_port_bw = configuration.tile_info[tile].avail_north_port_bw
                    = MBinBYTES(str_to_ull(north_port_bw.c_str()));

            std::string west_port_bw = child->first_attribute("west_port_bw")->value();
            configuration.tile_info[tile].west_port_bw = configuration.tile_info[tile].avail_west_port_bw
                    = MBinBYTES(str_to_ull(west_port_bw.c_str()));

            std::string east_port_bw = child->first_attribute("east_port_bw")->value();
            configuration.tile_info[tile].east_port_bw = configuration.tile_info[tile].avail_east_port_bw
                    = MBinBYTES(str_to_ull(east_port_bw.c_str()));

            std::string south_port_bw = child->first_attribute("south_port_bw")->value();
            configuration.tile_info[tile].south_port_bw = configuration.tile_info[tile].avail_south_port_bw
                    = str_to_ull(south_port_bw.c_str());

            std::string local_port_bw = child->first_attribute("local_port_bw")->value();
            configuration.tile_info[tile].local_port_bw = configuration.tile_info[tile].avail_local_port_bw
                    = MBinBYTES(str_to_ull(local_port_bw.c_str()));

            if (configuration.tile_info[tile].memory_size > 0) {
                configuration.tile_info[tile].first_memory_slot = (hn_rscmgt_memory_slot_t *) malloc(
                        sizeof(hn_rscmgt_memory_slot_t));
                configuration.tile_info[tile].first_memory_slot->size = configuration.tile_info[tile].memory_size;
                configuration.tile_info[tile].first_memory_slot->start_address = last_addr;
                configuration.tile_info[tile].first_memory_slot->end_address =
                        configuration.tile_info[tile].first_memory_slot->start_address +
                        configuration.tile_info[tile].memory_size-1;
                last_addr +=configuration.tile_info[tile].memory_size;
                configuration.tile_info[tile].first_memory_slot->next_memory_slot = nullptr;
            } else {
                configuration.tile_info[tile].first_memory_slot = nullptr;
            }
        }

        file.close();
    }


    uint32_t read_num(int *number, char *result) {

        std::string input;
        std::string::size_type sz;
        std::getline(std::cin, input);
        if (!input.empty()) {
            *number = std::stoi(input, &sz);
        }
        sprintf(result, "%d", *number);
        return 0;
    }



    uint32_t
    ResourcesManager::get_num_tiles(uint32_t *num_tiles, uint32_t *num_tiles_x, uint32_t *num_tiles_y) {

        if (configuration.num_tiles == 0 || configuration.num_cols == 0 || configuration.num_rows == 0
            || configuration.num_tiles != configuration.num_cols * configuration.num_rows) {
            return HN_TILE_DOES_NOT_EXIST;
        }
        *num_tiles = configuration.num_tiles;
        *num_tiles_x = configuration.num_cols;
        *num_tiles_y = configuration.num_rows;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::get_tile_info(uint32_t tile, hn_rscmgt_tile_info_t *data) {
        if (tile >= configuration.num_tiles) {
            return HN_TILE_DOES_NOT_EXIST;
        }

        *data = configuration.tile_info[tile];
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::get_memory_size(uint32_t tile,  uint32_t *size) {
        if (tile >= configuration.num_tiles) {
            return HN_TILE_DOES_NOT_EXIST;
        }
        *size = configuration.tile_info[tile].memory_size;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::get_memory_size(unsigned long long *size) {
        unsigned long long mem_size = 0;
        for (uint32_t i = 0; i < configuration.num_tiles; i++) {
            mem_size += configuration.tile_info[i].memory_size;
        }
        *size = mem_size;
        return HN_SUCCEEDED;
    }

    uint32_t
    ResourcesManager::get_memory(uint32_t tile, uint32_t *size, uint32_t *free,
                                 uint32_t *starting_addr) {
        if (tile >= configuration.num_tiles) {
            return HN_TILE_DOES_NOT_EXIST;
        }
        *size = configuration.tile_info[tile].memory_size;
        *free = configuration.tile_info[tile].free_memory;
        *starting_addr = configuration.tile_info[tile].first_memory_slot->start_address;
        return HN_SUCCEEDED;
    }

    uint32_t
    ResourcesManager::get_free_memory(uint32_t tile, uint32_t size, uint32_t *starting_addr) {

        if (tile >= configuration.num_tiles) {
            return HN_TILE_DOES_NOT_EXIST;
        }

        if (configuration.tile_info[tile].memory_size == 0) {
            return HN_MEMORY_NOT_PRESENT_IN_TILE;
        }


        //First Fit
        auto mem = configuration.tile_info[tile].first_memory_slot;

        while ((mem != nullptr) && (mem->size < size)) {
            mem = (hn_rscmgt_memory_slot_t *) mem->next_memory_slot;
        }


        if (mem == nullptr || mem->size < size) {
            *starting_addr = 0;
            return HN_NOT_ENOUGH_MEMORY_AVAILABLE;
        } else {
            *starting_addr = mem->start_address;
            return HN_SUCCEEDED;
        }
    }


    uint32_t ResourcesManager::is_tile_assigned(uint32_t tile, uint32_t *avail) {
        if (tile >= configuration.num_tiles) {
            *avail = 0;
            return HN_TILE_DOES_NOT_EXIST;
        }

        *avail = configuration.tile_info[tile].assigned;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::set_tile_assigned(uint32_t tile) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        configuration.tile_info[tile].assigned = 1;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::set_tile_avail(uint32_t tile) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        configuration.tile_info[tile].assigned = 0;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::get_available_read_memory_bw(uint32_t tile, unsigned long long *bw) {
        if (tile >= configuration.num_tiles) {
            *bw = 0;
            return HN_TILE_DOES_NOT_EXIST;
        }

        *bw = configuration.tile_info[tile].avail_read_memory_bw;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::get_available_write_memory_bw(uint32_t tile, unsigned long long *bw) {
        if (tile >= configuration.num_tiles) {
            *bw = 0;
            return HN_TILE_DOES_NOT_EXIST;
        }

        *bw = configuration.tile_info[tile].avail_write_memory_bw;
        return HN_SUCCEEDED;
    }

    uint32_t
    ResourcesManager::get_available_router_bw(uint32_t tile, uint32_t port, unsigned long long *bw) {
        if (tile >= configuration.num_tiles) {
            *bw = 0;
            return HN_TILE_DOES_NOT_EXIST;
        }
        switch (port) {
            case HN_NORTH_PORT:
                *bw = configuration.tile_info[tile].avail_north_port_bw;
                break;
            case HN_EAST_PORT:
                *bw = configuration.tile_info[tile].avail_east_port_bw;
                break;
            case HN_WEST_PORT:
                *bw = configuration.tile_info[tile].avail_west_port_bw;
                break;
            case HN_SOUTH_PORT:
                *bw = configuration.tile_info[tile].avail_south_port_bw;
                break;
            case HN_LOCAL_PORT:
                *bw = configuration.tile_info[tile].avail_local_port_bw;
                break;
        }
        return HN_SUCCEEDED;
    }

    /*
 * \brief This function delivers the available network bandwidth between two tiles
 */
    uint32_t ResourcesManager::get_available_network_bw(uint32_t tile_src, uint32_t tile_dst, unsigned long long *bw){
        //XY route for HN_MAX_ROUTER_PORTS = 4

        uint32_t num_tiles = configuration.num_tiles,
                 num_tiles_x = configuration.num_cols,
                 num_tiles_y = configuration.num_rows;

        uint32_t tile_cur = tile_src, tile_cur_x, tile_cur_y;
        uint32_t tile_dst_x = tile_dst % num_tiles_x;
        uint32_t tile_dst_y = tile_dst / num_tiles_x;
        uint32_t port = 0, status, found = 0;

        unsigned long long bw_cur = 0, bw_avail = 0;
        do {
            tile_cur_x = tile_cur % num_tiles_x;
            tile_cur_y = tile_cur / num_tiles_x;

            if (tile_cur_x < tile_dst_x) port = HN_EAST_PORT;
            else if (tile_cur_x > tile_dst_x) port = HN_WEST_PORT;
            else if (tile_cur_y < tile_dst_y) port = HN_SOUTH_PORT;
            else if (tile_cur_y > tile_dst_y) port = HN_NORTH_PORT;
            else port = HN_LOCAL_PORT;

            status =  get_available_router_bw(tile_cur,port, &bw_cur);

            if (status!=HN_SUCCEEDED) {
                return status;
            }

            if (found == 0) {
                bw_avail = bw_cur;
                found = 1;
            }

            if (bw_cur < bw_avail)
                bw_avail = bw_cur;

            if (port == HN_EAST_PORT) tile_cur++;
            else if (port == HN_WEST_PORT) tile_cur--;
            else if (port == HN_NORTH_PORT) tile_cur -= num_tiles_x;
            else if (port == HN_SOUTH_PORT) tile_cur += num_tiles_x;

        } while (tile_cur != tile_dst);

        *bw = bw_avail;
        return HN_SUCCEEDED;

    }


    uint32_t ResourcesManager::reserve_read_memory_bw(uint32_t tile, unsigned long long bw) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        if ((configuration.tile_info[tile].avail_read_memory_bw - bw) < 0) {
            return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
        }
        configuration.tile_info[tile].avail_read_memory_bw -= bw;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::reserve_write_memory_bw(uint32_t tile, unsigned long long bw) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        if ((configuration.tile_info[tile].avail_write_memory_bw - bw) < 0) {
            return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
        }
        configuration.tile_info[tile].avail_write_memory_bw -= bw;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::reserve_router_bw(uint32_t tile, uint32_t port, unsigned long long bw) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        switch (port) {
            case HN_NORTH_PORT :
                if ((configuration.tile_info[tile].avail_north_port_bw - bw) < 0) {
                    return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
                }
                configuration.tile_info[tile].avail_north_port_bw -= bw;
                break;
            case HN_EAST_PORT :
                if ((configuration.tile_info[tile].avail_east_port_bw - bw) < 0) {
                    return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
                }
                configuration.tile_info[tile].avail_east_port_bw -= bw;
                break;
            case HN_WEST_PORT :
                if ((configuration.tile_info[tile].avail_west_port_bw - bw) < 0) {
                    return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
                }
                configuration.tile_info[tile].avail_west_port_bw -= bw;
                break;
            case HN_SOUTH_PORT :
                if ((configuration.tile_info[tile].avail_south_port_bw - bw) < 0) {
                    return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
                }
                configuration.tile_info[tile].avail_south_port_bw -= bw;
                break;
            case HN_LOCAL_PORT:
                if ((configuration.tile_info[tile].avail_local_port_bw - bw) < 0) {
                    return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
                }
                configuration.tile_info[tile].avail_local_port_bw -= bw;
                break;

        }
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::reserve_network_bw(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw){
        //XY route for HN_MAX_ROUTER_PORTS = 4

        unsigned long long bw_avail = 0;
        auto err = get_available_network_bw(tile_src, tile_dst, & bw_avail);
        if (err!=HN_SUCCEEDED)
            return err;

        if (bw_avail < bw)
            return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;

        uint32_t num_tiles = configuration.num_tiles,
                num_tiles_x = configuration.num_cols,
                num_tiles_y = configuration.num_rows;


        uint32_t tile_cur = tile_src, tile_cur_x, tile_cur_y;
        uint32_t tile_dst_x = tile_dst % num_tiles_x;
        uint32_t tile_dst_y = tile_dst / num_tiles_x;
        uint32_t port = 0;

        do {
            tile_cur_x = tile_cur % num_tiles_x;
            tile_cur_y = tile_cur / num_tiles_x;

            if (tile_cur_x < tile_dst_x) port = HN_EAST_PORT;
            else if (tile_cur_x > tile_dst_x) port = HN_WEST_PORT;
            else if (tile_cur_y < tile_dst_y) port = HN_SOUTH_PORT;
            else if (tile_cur_y > tile_dst_y) port = HN_NORTH_PORT;

            err =  reserve_router_bw(tile_cur, port, bw);

            if (err!=HN_SUCCEEDED) {
                release_network_bw (tile_src, tile_cur, bw);
                return err;
            }

            if (port == HN_EAST_PORT) tile_cur++;
            else if (port == HN_WEST_PORT) tile_cur--;
            else if (port == HN_NORTH_PORT) tile_cur -= num_tiles_x;
            else if (port == HN_SOUTH_PORT) tile_cur += num_tiles_x;

        } while (tile_cur != tile_dst);

        return HN_SUCCEEDED;
    }


    uint32_t ResourcesManager::release_read_memory_bw(uint32_t tile, unsigned long long bw) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        configuration.tile_info[tile].avail_read_memory_bw += bw;
        if (configuration.tile_info[tile].avail_read_memory_bw > configuration.tile_info[tile].read_memory_bw) {
            return HN_WRONG_BANDWIDTH_SETTING;
        }
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::release_write_memory_bw(uint32_t tile, unsigned long long bw) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        configuration.tile_info[tile].avail_write_memory_bw += bw;
        if (configuration.tile_info[tile].avail_write_memory_bw > configuration.tile_info[tile].write_memory_bw) {
            return HN_WRONG_BANDWIDTH_SETTING;
        }
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::release_router_bw(uint32_t tile, uint32_t port, unsigned long long bw) {
        if (tile >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;

        switch (port) {
            case HN_NORTH_PORT :
                configuration.tile_info[tile].avail_north_port_bw += bw;
                if (configuration.tile_info[tile].avail_north_port_bw > configuration.tile_info[tile].north_port_bw) {
                    return HN_WRONG_BANDWIDTH_SETTING;
                }
                break;
            case HN_EAST_PORT :
                configuration.tile_info[tile].avail_east_port_bw += bw;
                if (configuration.tile_info[tile].avail_east_port_bw > configuration.tile_info[tile].east_port_bw) {
                    return HN_WRONG_BANDWIDTH_SETTING;
                }
                break;
            case HN_WEST_PORT :
                configuration.tile_info[tile].avail_west_port_bw += bw;
                if (configuration.tile_info[tile].avail_west_port_bw > configuration.tile_info[tile].west_port_bw) {
                    return HN_WRONG_BANDWIDTH_SETTING;
                }
                break;
            case HN_SOUTH_PORT :
                configuration.tile_info[tile].avail_south_port_bw += bw;
                if (configuration.tile_info[tile].avail_south_port_bw > configuration.tile_info[tile].south_port_bw) {
                    return HN_WRONG_BANDWIDTH_SETTING;
                }
                break;
            case HN_LOCAL_PORT :
                configuration.tile_info[tile].avail_local_port_bw += bw;
                if (configuration.tile_info[tile].avail_local_port_bw > configuration.tile_info[tile].local_port_bw) {
                    return HN_WRONG_BANDWIDTH_SETTING;
                }
                break;
        }
        return HN_SUCCEEDED;
    }


    uint32_t ResourcesManager::release_network_bw(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw){
        //XY route for HN_MAX_ROUTER_PORTS = 4

        uint32_t num_tiles = configuration.num_tiles,
                num_tiles_x = configuration.num_cols,
                num_tiles_y = configuration.num_rows;


        uint32_t tile_cur = tile_src, tile_cur_x, tile_cur_y;
        uint32_t tile_dst_x = tile_dst % num_tiles_x;
        uint32_t tile_dst_y = tile_dst / num_tiles_x;
        uint32_t port=0;

        do {
            tile_cur_x = tile_cur % num_tiles_x;
            tile_cur_y = tile_cur / num_tiles_x;

            if (tile_cur_x < tile_dst_x) port = HN_EAST_PORT;
            else if (tile_cur_x > tile_dst_x) port = HN_WEST_PORT;
            else if (tile_cur_y < tile_dst_y) port = HN_SOUTH_PORT;
            else if (tile_cur_y > tile_dst_y) port = HN_NORTH_PORT;

            auto err =  release_router_bw(tile_cur,port, bw);

            if (err!=HN_SUCCEEDED) {
                return err;
            }

            if (port == HN_EAST_PORT) tile_cur++;
            else if (port == HN_WEST_PORT) tile_cur--;
            else if (port == HN_NORTH_PORT) tile_cur -= num_tiles_x;
            else if (port == HN_SOUTH_PORT) tile_cur += num_tiles_x;

        } while (tile_cur != tile_dst);

        return HN_SUCCEEDED;
    }


    uint32_t ResourcesManager::get_available_read_cluster_bw(unsigned long long *bw) {

        *bw = configuration.avail_read_cluster_bw;
        return HN_SUCCEEDED;

    }

    uint32_t ResourcesManager::get_available_write_cluster_bw(unsigned long long *bw) {

        *bw = configuration.avail_write_cluster_bw;
        return HN_SUCCEEDED;
    }


    uint32_t ResourcesManager::reserve_read_cluster_bw(unsigned long long bw) {

        if ((configuration.avail_read_cluster_bw - bw) < 0) {
            return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
        }
        configuration.avail_read_cluster_bw -= bw;
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::reserve_write_cluster_bw(unsigned long long bw) {

        if ((configuration.avail_write_cluster_bw - bw) < 0) {
            return HN_NOT_ENOUGH_BANDWIDTH_AVAILABLE;
        }
        configuration.avail_write_cluster_bw -= bw;
        return HN_SUCCEEDED;
    }


    uint32_t ResourcesManager::release_read_cluster_bw(unsigned long long bw) {

        configuration.avail_read_cluster_bw += bw;
        if (configuration.avail_write_cluster_bw > configuration.write_cluster_bw) {
            return HN_WRONG_BANDWIDTH_SETTING;
        }
        return HN_SUCCEEDED;
    }

    uint32_t ResourcesManager::release_write_cluster_bw(unsigned long long bw) {

        configuration.avail_read_cluster_bw += bw;
        if (configuration.avail_read_cluster_bw > configuration.read_cluster_bw) {
            return HN_WRONG_BANDWIDTH_SETTING;
        }
        return HN_SUCCEEDED;
    }


    uint32_t ResourcesManager::checkMemory(uint32_t tile, uint32_t size, uint32_t *tile_mem,
                                           uint32_t *starting_addr) {

        auto err = get_free_memory(tile, size, starting_addr);
        if (err == HN_SUCCEEDED) {
            *tile_mem = tile;
            return 1;
        } else
            return 0;
    }

    inline uint32_t isValid(uint32_t tile_x, uint32_t tile_y, uint32_t num_tiles_x, uint32_t num_tiles_y) {
        if (tile_x < num_tiles_x && tile_y < num_tiles_y)
            return 1;
        else
            return 0;
    }


    uint32_t ResourcesManager::find_memory(uint32_t tile, uint32_t size, uint32_t *tile_mem,
                                           uint32_t *starting_addr) {


        uint32_t tile_cur = tile;

        auto err = get_free_memory(tile, size, starting_addr);

        if (err == HN_SUCCEEDED) {
            *tile_mem = tile_cur;
            return HN_SUCCEEDED;
        }

        uint32_t num_tiles_x = configuration.num_cols,
                num_tiles_y = configuration.num_rows;


        //von Neumann Neighborhood for HN_MAX_ROUTER_PORTS = 4
        uint32_t d, i, j, d_max = num_tiles_x + num_tiles_y - 2;

        uint32_t tile_x = tile % num_tiles_x;
        uint32_t tile_y = tile / num_tiles_x;


        unsigned long long bw;

        for (d = 1; d < d_max; d++) {
            for (i = d, j = 0; i >= 0 && j <= d; i--, j++) {
                bw = 0;
                if (i > 0 && j > 0) {
                    tile_cur = tile - i * num_tiles_x + j;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x + j, tile_y - i, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)) {
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                    bw = 0;

                    tile_cur = tile - i * num_tiles_x - j;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x - j, tile_y - i, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                    bw = 0;

                    tile_cur = tile + i * num_tiles_x - j;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x - j, tile_y + i, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }

                    bw = 0;
                    tile_cur = tile + i * num_tiles_x + j;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x + j, tile_y + i, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                } else if (j == 0) {
                    tile_cur = tile - i * num_tiles_x;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x, tile_y - i, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                    bw = 0;

                    tile_cur = tile + i * num_tiles_x;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x, tile_y + i, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                } else if (i == 0) {
                    tile_cur = tile + j;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x + j, tile_y, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                    bw = 0;

                    tile_cur = tile - j;
                    get_available_network_bw(tile, tile_cur, &bw);
                    if (bw > 0 &&
                        gnemu::isValid(tile_x - j, tile_y, num_tiles_x, num_tiles_y) &&
                        checkMemory(tile_cur, size, tile_mem, starting_addr)){
                        gnemu_log->info("find_memory: tile %v, address %v, size %v", *tile_mem, *starting_addr, size);
                        return HN_SUCCEEDED;
                    }
                }
            }
        }


        *starting_addr = 0;
        *tile_mem = 0;
        gnemu_log->info("find_memory: HN_NOT_ENOUGH_MEMORY_AVAILABLE");
        return HN_NOT_ENOUGH_MEMORY_AVAILABLE;
    }

    uint32_t ResourcesManager::allocate_memory(uint32_t tile, uint32_t addr, uint32_t size) {

        if (configuration.tile_info[tile].memory_size == 0) {
            return HN_MEMORY_NOT_PRESENT_IN_TILE;
        }
        auto mem = configuration.tile_info[tile].first_memory_slot;
        auto prev = configuration.tile_info[tile].first_memory_slot;

        //find memory slot with start_address = addr
        while (mem->start_address != addr) {
            prev = mem;
            mem = (hn_rscmgt_memory_slot_t *) mem->next_memory_slot;

            if (mem == nullptr) {
                return HN_FIND_MEMORY_ERROR;
            }
        }


        if (mem->size < size) {
            return HN_NOT_ENOUGH_MEMORY_AVAILABLE;
        }


        configuration.tile_info[tile].free_memory -= size;

        //delete full slot
        if ((prev != mem) && (mem->size == size)) {
            prev->next_memory_slot = mem->next_memory_slot;
            free(mem);
        } else { //change slot (no free space: start = end+1, size = 0
            mem->start_address += size;
            mem->size -= size;
        }

        gnemu_log->info("allocate_memory: tile %v, address %v, size %v", tile, addr, size);
        return HN_SUCCEEDED;

    }


    uint32_t ResourcesManager::release_memory(uint32_t tile, uint32_t addr, uint32_t size) {

        gnemu_log->info("release_memory: tile %v, address %v, size %v", tile, addr, size);
        if (configuration.tile_info[tile].memory_size == 0) {
            return HN_MEMORY_NOT_PRESENT_IN_TILE;
        }

        auto mem = configuration.tile_info[tile].first_memory_slot;
        auto prev = configuration.tile_info[tile].first_memory_slot;

        configuration.tile_info[tile].free_memory += size;

        while (mem != nullptr) {

            if ((prev->start_address < addr) && (mem->start_address > addr)) {
                if ((prev->end_address + 1) == addr && (addr + size) != mem->start_address) {
                    prev->size += size;
                    prev->end_address += size;
                    return HN_SUCCEEDED;
                } else if ((prev->end_address + 1) != addr && (addr + size) == mem->start_address) {
                    mem->size += size;
                    mem->start_address = addr;
                    return HN_SUCCEEDED;
                } else if ((prev->end_address + 1) == addr && (addr + size) == mem->start_address) {
                    prev->size += size + mem->size;
                    prev->end_address = mem->end_address;
                    prev->next_memory_slot = mem->next_memory_slot;
                    free(mem);
                    return HN_SUCCEEDED;
                } else if ((prev->end_address + 1) != addr && (addr + size) != mem->start_address) {
                    auto slot = (hn_rscmgt_memory_slot_t *) malloc(sizeof(hn_rscmgt_memory_slot_t));
                    slot->size = size;
                    slot->start_address = addr;
                    slot->end_address = addr + size - 1;
                    slot->next_memory_slot = mem;
                    prev->next_memory_slot = slot;
                    return HN_SUCCEEDED;
                }

            } else {
                prev = mem;
                mem = (hn_rscmgt_memory_slot_t *) mem->next_memory_slot;
            }

        }

        if ((addr + size) == configuration.tile_info[tile].first_memory_slot->start_address) {
            configuration.tile_info[tile].first_memory_slot->size += size;
            configuration.tile_info[tile].first_memory_slot->start_address = addr;
            return HN_SUCCEEDED;
        } else if ((prev->end_address + 1) == addr) {
            prev->size += size;
            prev->end_address += size;
            return HN_SUCCEEDED;
        } else {
            auto slot = (hn_rscmgt_memory_slot_t *) malloc(sizeof(hn_rscmgt_memory_slot_t));
            slot->size = size;
            slot->start_address = addr;
            slot->end_address = addr + size - 1;
            if (addr > prev->start_address) {
                prev->next_memory_slot = slot;
                slot->next_memory_slot = nullptr;
            } else {
                mem = configuration.tile_info[tile].first_memory_slot;
                configuration.tile_info[tile].first_memory_slot = slot;
                slot->next_memory_slot = mem;
            }
            return HN_SUCCEEDED;
        }

        return HN_FIND_MEMORY_ERROR;
    }

//distance
    uint32_t ResourcesManager::get_network_distance(uint32_t tile_src, uint32_t tile_dst, uint32_t *dst) {
        //XY route for HN_MAX_ROUTER_PORTS = 4
        if (tile_src >= configuration.num_tiles || tile_dst >= configuration.num_tiles)
            return HN_TILE_DOES_NOT_EXIST;


        uint32_t tile_cur_x = tile_src % configuration.num_cols;
        uint32_t tile_cur_y = tile_src / configuration.num_cols;
        uint32_t tile_dst_x = tile_dst % configuration.num_cols;
        uint32_t tile_dst_y = tile_dst / configuration.num_cols;


        *dst = ((tile_cur_x > tile_dst_x) ? (tile_cur_x - tile_dst_x) :
                (tile_dst_x - tile_cur_x)) + ((tile_cur_y > tile_dst_y) ?
                                              (tile_cur_y - tile_dst_y) : (tile_dst_y - tile_cur_y));
        return HN_SUCCEEDED;

    }

    uint32_t ResourcesManager::checkType(uint32_t tile_cur, uint32_t *num_tiles_cur, uint32_t num_tiles,
                                         uint32_t *tiles_cur, const uint32_t *types) {

        if (*num_tiles_cur >= num_tiles) return 0;
        for (uint32_t i = 0; i < num_tiles; i++) {
            if (configuration.tile_info[tile_cur].assigned == 0 &&
                configuration.tile_info[tile_cur].preassigned == 0 &&
                configuration.tile_info[tile_cur].type == types[i] &&
                tiles_cur[i] == configuration.num_tiles) {
                tiles_cur[i] = tile_cur;
                configuration.tile_info[tile_cur].preassigned = 1;
                *num_tiles_cur += 1;
                return 1;
            }
        }
        return 0;
    }

    void update_bounds(uint32_t cur_x, uint32_t cur_y, uint32_t *start_x, uint32_t *start_y, uint32_t *end_x,
                       uint32_t *end_y) {

        *start_x = (cur_x < *start_x) ? cur_x : *start_x;
        *start_y = (cur_y < *start_y) ? cur_y : *start_y;
        *end_x = (cur_x > *end_x) ? cur_x : *end_x;
        *end_y = (cur_y > *end_y) ? cur_y : *end_y;

    }


    uint32_t ResourcesManager::find_single_units_set(uint32_t tile, uint32_t num_tiles, uint32_t types[],
                                                     uint32_t **tiles_dst, uint32_t *dst) {

        //von Neumann Neighborhood for HN_MAX_ROUTER_PORTS = 4

        uint32_t *tiles_dst_cur = *tiles_dst;

        uint32_t num_tiles_x = configuration.num_cols,
                num_tiles_y = configuration.num_rows,
                d, i, j, d_max = num_tiles_x + num_tiles_y - 2, sum_dst = *dst;

        uint32_t num_tiles_cur = 0, tile_cur, tile_cur_x, tile_cur_y;
        uint32_t tile_x = tile % num_tiles_x;
        uint32_t tile_y = tile / num_tiles_x;
        uint32_t start_x = tile_x, start_y = tile_y;
        uint32_t end_x = tile_x, end_y = tile_y;


        for (d = 0; d < d_max && num_tiles_cur < num_tiles; d++) {
            for (i = d, j = 0; i >= 0 && j <= d && num_tiles_cur < num_tiles; i--, j++) {
                if (i > 0 && j > 0) {
                    tile_cur = tile - i * num_tiles_x + j;
                    tile_cur_x = tile_x + j;
                    tile_cur_y = tile_y - i;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                    tile_cur = tile - i * num_tiles_x - j;
                    tile_cur_x = tile_x - j;
                    tile_cur_y = tile_y - i;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                    tile_cur = tile + i * num_tiles_x - j;
                    tile_cur_x = tile_x - j;
                    tile_cur_y = tile_y + i;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                    tile_cur = tile + i * num_tiles_x + j;
                    tile_cur_x = tile_x + j;
                    tile_cur_y = tile_y + i;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                } else if (j == 0) {
                    tile_cur = tile - i * num_tiles_x;
                    tile_cur_x = tile_x;
                    tile_cur_y = tile_y - i;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                    tile_cur = tile + i * num_tiles_x;
                    tile_cur_x = tile_x;
                    tile_cur_y = tile_y + i;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                } else if (i == 0) {
                    tile_cur = tile + j;
                    tile_cur_x = tile_x + j;
                    tile_cur_y = tile_y;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                    tile_cur = tile - j;
                    tile_cur_x = tile_x - j;
                    tile_cur_y = tile_y;
                    if (gnemu::isValid(tile_cur_x, tile_cur_y, num_tiles_x, num_tiles_y) &&
                        checkType(tile_cur, &num_tiles_cur, num_tiles, tiles_dst_cur, types)) {
                        sum_dst += d;
                    }
                }
            }
        }

        *dst = sum_dst;

//      clean preassigment
        for (uint32_t k = 0; k < num_tiles_cur; k++) {
            configuration.tile_info[tiles_dst_cur[k]].preassigned = 0;
        }


        if (num_tiles_cur == num_tiles) {
            return HN_SUCCEEDED;
        } else {
            return HN_PARTITION_NOT_FOUND;
        }

    }

    uint32_t ResourcesManager::find_units_set(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t **tiles_dst,
                                uint32_t *types_dst){

        if (num_tiles == 0) {
            return HN_SUCCEEDED;
        }

        uint32_t **tiles_cur, **tiles_min = nullptr;
        auto tiles1 = (uint32_t *) malloc(sizeof(*tiles_dst) * (num_tiles));
        auto tiles2 = (uint32_t *) malloc(sizeof(*tiles_dst) * (num_tiles));

        tiles_cur = &tiles1;

        uint32_t i, sum_dst = 0, min_dst = 0, found = 0, num_tiles_cur, num_tiles_min;

        for (uint32_t tile_cur = 0; tile_cur < configuration.num_tiles; tile_cur++) {


            if (configuration.tile_info[tile_cur].memory_size == 0) continue;
            if (configuration.tile_info[tile_cur].assigned == 1) continue;

            sum_dst = 0;
            for (i = 0; i < (num_tiles); i++) {
                (*tiles_cur)[i] = configuration.num_tiles;
            }

            auto err = find_single_units_set(tile_cur, num_tiles, types, tiles_cur, &sum_dst);

            if (err == HN_PARTITION_NOT_FOUND) continue;

            if (found == 0) {
                min_dst = sum_dst;
                tiles_min = tiles_cur;
                tiles_cur = &tiles2;
                found = 1;
            } else if (min_dst > sum_dst) {
                min_dst = sum_dst;
                auto tmp = tiles_min;
                tiles_min = tiles_cur;
                tiles_cur = tmp;
            }
        }


        if (found) {
            *tiles_dst = *tiles_min;
            gnemu_log->info("find_units_set: num tiles %v", num_tiles);
            if (&tiles1 != tiles_min) {
                free(tiles1);
            } else {
                free(tiles2);
            }
            return HN_SUCCEEDED;
        } else {
            free(tiles1);
            free(tiles2);
            gnemu_log->info("find_units_set: HN_PARTITION_NOT_FOUND");
            return HN_PARTITION_NOT_FOUND;
        }

    }


    uint32_t ResourcesManager::find_units_sets(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t ***tiles_dst,
                             uint32_t ***types_dst, uint32_t *num){

    if (num_tiles == 0) {
            return HN_SUCCEEDED;
        }

        uint32_t i, sum_dst = 0, num_cur = 0, num_tiles_cur;
        auto tiles_cur = (uint32_t *) malloc(sizeof(**tiles_dst) * (num_tiles));
        uint32_t **tiles_dst_cur = nullptr, **types_dst_cur = nullptr;

        for (i = 0; i < num_tiles; i++)
            tiles_cur[i] = configuration.num_tiles;

        for (uint32_t tile_cur = 0; tile_cur < configuration.num_tiles; tile_cur++) {

            if (configuration.tile_info[tile_cur].memory_size == 0) continue;
            if (configuration.tile_info[tile_cur].assigned == 1) continue;

            sum_dst = 0;

            auto err = find_single_units_set(tile_cur, num_tiles, types, &tiles_cur, &sum_dst);

            if (err == HN_PARTITION_NOT_FOUND) continue;

            num_cur += 1;

            tiles_dst_cur = (uint32_t **) realloc(tiles_dst_cur, sizeof(*tiles_dst_cur) * num_cur);
            types_dst_cur = (uint32_t **) realloc(types_dst_cur, sizeof(*types_dst_cur) * num_cur);

            if (tiles_dst_cur != nullptr && types_dst_cur != nullptr) {
                tiles_dst_cur[num_cur - 1] = (uint32_t *) malloc(num_tiles * sizeof(uint32_t));
                types_dst_cur[num_cur - 1] = (uint32_t *) malloc(num_tiles * sizeof(uint32_t));
            } else {
                for (i = 0; i < num_cur; i++) {
                    free(tiles_dst_cur[i]);
                    free(types_dst_cur[i]);
                }
                free(tiles_dst_cur);
                free(types_dst_cur);
                return HN_PARTITION_NOT_FOUND;
            }

            for (i = 0; i < num_tiles; i++) {
                tiles_dst_cur[num_cur - 1][i] = tiles_cur[i];
                types_dst_cur[num_cur - 1][i] = configuration.tile_info[tiles_cur[i]].type;
                tiles_cur[i] = configuration.num_tiles;
            }

        }

        *num = num_cur;

        if (num_cur > 0) {
            *tiles_dst = tiles_dst_cur;
            *types_dst = types_dst_cur;
            gnemu_log->info("find_units_sets: num tiles %v", num_cur);
            return HN_SUCCEEDED;
        } else {
            free(tiles_dst_cur);
            free(types_dst_cur);
            gnemu_log->info("find_units_sets: HN_PARTITION_NOT_FOUND");
            return HN_PARTITION_NOT_FOUND;
        }

    }



/*
 * \brief This function reserves a set of tiles
 */
    uint32_t ResourcesManager::reserve_units_set(uint32_t num_tiles, uint32_t *tiles){

        uint32_t err, status;
        uint32_t num_tiles_x = configuration.num_cols,
                num_tiles_y = configuration.num_rows;
        uint32_t tile_x = tiles[0] % num_tiles_x;
        uint32_t tile_y = tiles[0] / num_tiles_x;
        uint32_t start_x = tile_x, start_y = tile_y;
        uint32_t end_x = tile_x, end_y = tile_y;



        for (uint32_t i = 0; i<num_tiles; i++){
            err = is_tile_assigned(tiles[i], &status);


            if (err!=HN_SUCCEEDED) {
                gnemu_log->info("reserve_units_set: num tiles %v, error, release of %v already reserved tiles", num_tiles, i );
                for (uint32_t j = 0; j<i; j++){
                        auto err = set_tile_avail(tiles[j]);
                }

                //TODO error code?
                return HN_RSC_ERROR;
            }

            if (status==0){
                set_tile_assigned(tiles[i]);
                gnemu::update_bounds(tiles[i] % num_tiles_x, tiles[i] / num_tiles_x, &start_x, &start_y, &end_x, &end_y);
            } else {
                gnemu_log->info("reserve_units_set: num tiles %v, unit %v is currently assigned, release of %v already reserved tiles", num_tiles, tiles[i], i );
                for (uint32_t j = 0; j<i; j++){
                    auto err = set_tile_avail(tiles[j]);
                }
                //TODO error code?
                return HN_RSC_ERROR;
            }
        }

//      include all tiles in isolated area
        uint32_t num_tiles_final = (uint32_t)(abs(end_x - start_x) + 1) * (abs(end_y - start_y) + 1);
        if (num_tiles_final > num_tiles) {
                for (uint32_t i = start_y; i <= end_y; i++)
                    for (uint32_t j = start_x; j <= end_x; j++) {
                        if (configuration.tile_info[i * num_tiles_x + j].assigned == 0) {
                            configuration.tile_info[i * num_tiles_x + j].assigned =1;
                        }
                    }
        }

        gnemu_log->info("reserve_units_set: num tiles %v, units in isolated area: %v, start: [%v,%v], end: [%v,%v] ",num_tiles, num_tiles_final, start_x, start_y, end_x, end_y);
//      isolate area using bw
        if (start_y > 0){
            for (int i = start_x; i <= end_x; i++) {
                uint32_t bound_tile = (start_y - 1) * num_tiles_x + i;
                uint32_t cur_tile = (start_y) * num_tiles_x + i;
                configuration.tile_info[bound_tile].avail_south_port_bw = 0;
                configuration.tile_info[cur_tile].avail_north_port_bw = 0;
            }
        }

        if (end_y < (num_tiles_y-1)){
            for (int i = start_x; i <= end_x; i++) {
                uint32_t bound_tile = (end_y + 1) * num_tiles_x + i;
                uint32_t cur_tile = (end_y) * num_tiles_x + i;
                configuration.tile_info[bound_tile].avail_north_port_bw = 0;
                configuration.tile_info[cur_tile].avail_south_port_bw = 0;
            }
        }


        if (start_x > 0){
            for (int i = start_y; i <= end_y; i++) {
                uint32_t bound_tile = i * num_tiles_x + (start_x-1);
                uint32_t cur_tile = i * num_tiles_x + (start_x);
                configuration.tile_info[bound_tile].avail_east_port_bw = 0;
                configuration.tile_info[cur_tile].avail_west_port_bw = 0;
            }
        }

        if (end_x < (num_tiles_x-1)){
            for (int i = start_y; i <= end_y; i++) {
                uint32_t bound_tile = i * num_tiles_x + (end_x+1);
                uint32_t cur_tile = i * num_tiles_x + (end_x);
                configuration.tile_info[bound_tile].avail_west_port_bw = 0;
                configuration.tile_info[cur_tile].avail_east_port_bw = 0;
            }
        }

        return HN_SUCCEEDED;
    }

/*
 * \brief This function releases a set of tiles
 */
    uint32_t ResourcesManager::release_units_set(uint32_t num_tiles, uint32_t *tiles){

        uint32_t status = 0;
        uint32_t num_tiles_x = configuration.num_cols,
                num_tiles_y = configuration.num_rows;
        uint32_t tile_x = tiles[0] % num_tiles_x;
        uint32_t tile_y = tiles[0] / num_tiles_x;
        uint32_t start_x = tile_x, start_y = tile_y;
        uint32_t end_x = tile_x, end_y = tile_y;

        for (uint32_t i = 0; i<num_tiles; i++){

            is_tile_assigned(tiles[i], &status);

            if (status){
                auto err = set_tile_avail(tiles[i]);

            }
            gnemu::update_bounds(tiles[i] % num_tiles_x, tiles[i] / num_tiles_x, &start_x, &start_y, &end_x, &end_y);
        }

        //release all tiles in isolated area
        uint32_t num_tiles_final = (uint32_t)(abs(end_x - start_x) + 1) * (abs(end_y - start_y) + 1);
        if (num_tiles_final > num_tiles) {
            for (uint32_t i = start_y; i <= end_y; i++)
                for (uint32_t j = start_x; j <= end_x; j++) {
                    if (configuration.tile_info[i * num_tiles_x + j].assigned == 1) {
                        configuration.tile_info[i * num_tiles_x + j].assigned = 0;
                    }
                }
        }

        gnemu_log->info("release_units_set: %v, units in isolated area: %v, start: [%v,%v], end: [%v,%v] ", num_tiles, num_tiles_final,start_x, start_y, end_x, end_y);
//      release bw of bounds of isolated area
        if (start_y > 0){
            for (int i = start_x; i <= end_x; i++) {
                uint32_t bound_tile = (start_y - 1) * num_tiles_x + i;
                uint32_t cur_tile = (start_y) * num_tiles_x + i;
                if (configuration.tile_info[bound_tile].assigned == 0) {
                    configuration.tile_info[bound_tile].avail_south_port_bw = configuration.tile_info[bound_tile].south_port_bw;
                    configuration.tile_info[cur_tile].avail_north_port_bw = configuration.tile_info[cur_tile].north_port_bw;
                }
            }
        }

        if (end_y < (num_tiles_y-1)){
            for (int i = start_x; i <= end_x; i++) {
                uint32_t bound_tile = (end_y + 1) * num_tiles_x + i;
                uint32_t cur_tile = (end_y) * num_tiles_x + i;
                if (configuration.tile_info[bound_tile].assigned == 0) {
                    configuration.tile_info[bound_tile].avail_north_port_bw = configuration.tile_info[bound_tile].north_port_bw;
                    configuration.tile_info[cur_tile].avail_south_port_bw = configuration.tile_info[cur_tile].south_port_bw;
                }
            }
        }


        if (start_x > 0){
            for (int i = start_y; i <= end_y; i++) {
                uint32_t bound_tile = i * num_tiles_x + (start_x-1);
                uint32_t cur_tile = i * num_tiles_x + (start_x);
                if (configuration.tile_info[bound_tile].assigned == 0) {
                    configuration.tile_info[bound_tile].avail_east_port_bw = configuration.tile_info[bound_tile].east_port_bw;
                    configuration.tile_info[cur_tile].avail_west_port_bw = configuration.tile_info[cur_tile].west_port_bw;
                }
            }
        }

        if (end_x < (num_tiles_x-1)){
            for (int i = start_y; i <= end_y; i++) {
                uint32_t bound_tile = i * num_tiles_x + (end_x+1);
                uint32_t cur_tile = i * num_tiles_x + (end_x);
                if (configuration.tile_info[bound_tile].assigned == 0) {
                    configuration.tile_info[bound_tile].avail_west_port_bw = configuration.tile_info[bound_tile].west_port_bw;
                    configuration.tile_info[cur_tile].avail_east_port_bw = configuration.tile_info[cur_tile].east_port_bw;
                }
            }
        }

        return  HN_SUCCEEDED;
    }


}


